﻿namespace SlackBot
{
    using System;
    using System.Collections.Generic;
    using Quartz;
    using Quartz.Impl;
    using Quartz.Impl.Matchers;
    using Reminders;
    using TechTalk.JiraRestClient;
    using FluentDateTime;
    using MargieBot;

    public class QuartzScheduler
    {
        static ISchedulerFactory schedFact = new StdSchedulerFactory();
        private static QuartzScheduler _instance;
        private JiraConfiguration _config;
        private string _teamName;
        private string _usersToSlackMapping;
        private Bot _bot;
        private int _hourInterval;
        private int _allowedDays;

        private QuartzScheduler()
        {
        }

        public static QuartzScheduler Instance
        {
            get
            {
                if (_instance == null)
                {
                    _instance = new QuartzScheduler();
                }
                return _instance;
            }
        }

        public void Init(JiraConfiguration config, string teamName, string usersToSlackMapping, MargieBot.Bot bot,
            int hourInterval, int allowedDays)
        {
            _config = config;
            _teamName = teamName;
            _usersToSlackMapping = usersToSlackMapping;
            _bot = bot;
            _hourInterval = hourInterval;
            _allowedDays = allowedDays;
        }
        public void StartFillWorklogReminder()
        {
            // construct a scheduler factory
            // get a scheduler
            var sched = schedFact.GetScheduler();
            sched.UnscheduleJob(new TriggerKey("fillWorklogReminderTrigger", "reminders"));
            //start at next full minute
            DateTime startAtCalculated = DateTime.Now.At(15, 00);
            if (startAtCalculated >= DateTime.Now)
            {
                startAtCalculated = startAtCalculated.NextDay();
            }
            // define the job and tie it to our HelloJob class
            var job = JobBuilder.Create<FillWorklog>()
                .WithIdentity("FillWorklog", "reminders")
                .UsingJobData("JiraHost", _config.JiraHost)
                .UsingJobData("AllowedDays", _allowedDays)
                .UsingJobData("JiraUser", _config.Username)
                .UsingJobData("JiraPass", _config.Password)
                .UsingJobData("Team", _teamName)
                .UsingJobData("UsersToSlackMapping", _usersToSlackMapping)
                .Build();

            var trigger = TriggerBuilder.Create()
                .WithIdentity("fillWorklogReminderTrigger", "reminders")
                .StartAt(startAtCalculated)
                .WithSimpleSchedule(x => x
                //.WithIntervalInMinutes(3)
                .WithIntervalInHours(_hourInterval)
                .RepeatForever())
                .Build();

            sched.ScheduleJob(job, trigger);
            
            IJobListener myJobListener = new FillWorklogJobListener(_bot);
            ITriggerListener fillWorklogTriggerListener = new FillWorklogTriggerListener (new List<DayOfWeek> { DayOfWeek.Saturday, DayOfWeek.Sunday });
            sched.ListenerManager.AddJobListener(myJobListener, KeyMatcher<JobKey>.KeyEquals(new JobKey("FillWorklog", "reminders")));
            sched.ListenerManager.AddTriggerListener(fillWorklogTriggerListener, KeyMatcher<TriggerKey>.KeyEquals(new TriggerKey("fillWorklogReminderTrigger", "reminders")));
            sched.Start();
        }


        public void OneTimeWorkLogReminder(int allowedDays)
        {
            string triggerKey = $"fillWorklog1Time{Guid.NewGuid().ToString()}";
            var sched = schedFact.GetScheduler();
            sched.UnscheduleJob(new TriggerKey(triggerKey, "reminders"));
            //start at next full minute
            DateTime startAtCalculated = DateTime.Now.At(15, 00);
            if (startAtCalculated >= DateTime.Now)
            {
                startAtCalculated = startAtCalculated.NextDay();
            }
            // define the job and tie it to our HelloJob class
            string jobKey = $"FillWorklog{Guid.NewGuid().ToString()}";
            var job = JobBuilder.Create<FillWorklog>()
                .WithIdentity(jobKey, "reminders")
                .UsingJobData("JiraHost", _config.JiraHost)
                .UsingJobData("AllowedDays", allowedDays)
                .UsingJobData("JiraUser", _config.Username)
                .UsingJobData("JiraPass", _config.Password)
                .UsingJobData("Team", _teamName)
                .UsingJobData("UsersToSlackMapping", _usersToSlackMapping)
                .Build();

          
            var trigger = TriggerBuilder.Create()
                .WithIdentity(triggerKey, "reminders")
                .StartAt(startAtCalculated)
                .WithSimpleSchedule(x=>x.WithRepeatCount(0))
             .Build();

            sched.ScheduleJob(job, trigger);

            IJobListener myJobListener = new FillWorklogJobListener(_bot);
            ITriggerListener fillWorklogTriggerListener = new FillWorklogTriggerListener(new List<DayOfWeek> { DayOfWeek.Saturday, DayOfWeek.Saturday });
            sched.ListenerManager.AddJobListener(myJobListener, KeyMatcher<JobKey>.KeyEquals(new JobKey(jobKey, "reminders")));
            sched.ListenerManager.AddTriggerListener(fillWorklogTriggerListener, KeyMatcher<TriggerKey>.KeyEquals(new TriggerKey(triggerKey, "reminders")));
            sched.Start();
        }
    }
}
