﻿namespace SlackBot.Responders
{
    using MargieBot.Models;
    using MargieBot.Responders;

    public class JoseTestResponder :IResponder
    {
        public bool CanRespond(ResponseContext context)
        {
            return context.Message.Text == "JOSE TEST";
        }

        public BotMessage GetResponse(ResponseContext context)
        {
            return new BotMessage()
            {
                Text = "Hello Jose!"
            };
        }
    }
}
