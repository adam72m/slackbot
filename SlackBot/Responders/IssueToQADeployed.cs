﻿namespace SlackBot.Responders
{
    using System;
    using System.Collections.Generic;
    using System.Configuration;
    using System.Linq;
    using System.Text.RegularExpressions;
    using System.Threading.Tasks;
    using BitbucketIntegration;
    using JetBrains.Annotations;
    using MargieBot;
    using MargieBot.Models;
    using MargieBot.Responders;
    using Slackbot.Shared;
    using Slackbot.Shared.Logging;
    using TeamCityIntegration;

    public class IssueToQADeployed: IResponder
    {
        private readonly TeamCityConfiguration _teamCityConfiguration;
        private readonly BitbucketConfiguration _bitbucketConfiguration;
        private readonly string _branch;
        private readonly string _mainBuildTypeId;
        private readonly string _deployBuildTypeId;
        private readonly string _qaResponseChannel;
        private readonly int _maxRetries;
        private readonly TimeSpan _checkInterval;
        private readonly ILog _log;
        private readonly Bot _bot;
        readonly TC _tcIntegration;
        IBitBucketRepository _bitBucketRepository;
        readonly Scheduler _scheduler;
        private const string MESSAGE_PATTERN = @"RR-[\d]+";

        public IssueToQADeployed(TeamCityConfiguration teamCityConfiguration, IBitBucketRepository bitBucketRepository,
            string branch, string mainBuildTypeId, string qaResponseChannel, int maxRetries, TimeSpan checkInterval, ILog log, Bot bot)
        {
            _teamCityConfiguration = teamCityConfiguration;
            _branch = branch;
            _mainBuildTypeId = mainBuildTypeId;
            _qaResponseChannel = qaResponseChannel;
            _maxRetries = maxRetries;
            _checkInterval = checkInterval;
            _log = log;
            _bot = bot;

            _bitBucketRepository = bitBucketRepository;
            _tcIntegration = new TC(_teamCityConfiguration);
           
            _scheduler = new Scheduler(log);
        }

        public bool CanRespond(ResponseContext context)
        {
            return Regex.IsMatch(context.Message.Text, MESSAGE_PATTERN);
        }

        public void HandleIssue([NotNull]IssueInfo issue, [NotNull]string responseChannel)
        {
            var actionToSchedule = new SchedulerTask(() => CheckIfDeployed(issue, responseChannel), (x) => x.MaxRetryCount = 0, issue.Key)
            {
                RetryInterval = _checkInterval,
                MaxRetryCount = _maxRetries
            };
            try
            {
                _scheduler.ScheduleTask(actionToSchedule, TimeSpan.Zero);
            }
            catch (TaskAlreadyScheduledException taskAlreadyScheduledException)
            {
                _log.Log(LogLevel.Error, () => string.Format("Issue {0} is already scheduled!", issue.Key));
            }
        }
        public BotMessage GetResponse(ResponseContext context)
        {
            var jiraIssue = Regex.Match(context.Message.Text, MESSAGE_PATTERN).Value;
            var userName = context.UserNameCache[context.Message.User.ID];
            HandleIssue(new IssueInfo() {Key = jiraIssue}, userName);
            // margie bot implementation just won't answer
            return null;
        }

        public TaskResult CheckIfDeployed(IssueInfo issue, string responseChannel)
        {
            _log.Log(LogLevel.Debug, ()=> string.Format("QA:{0} checker triggered", issue));
            var data = _bitBucketRepository.GetLastCommitForJiraIssue(issue.Key, _branch);
            // commit not found
            if (data == null)
            {
                _bot.SendMessage(GetNoCommitMessage(issue), responseChannel);
                return TaskResult.RetryWithAction;
            }

            Task<string> build = null;
            try
            {
                build = _tcIntegration.GetBuildNumberWithChange(data.Hash, _mainBuildTypeId);
                build.Wait();
            }
            catch (Exception ex)
            {
                _log.Log(LogLevel.Error, () => string.Format("Issue {0}: failed to get builds"));
            }
            
            if (build != null && build.Status == TaskStatus.RanToCompletion )
            {
                if (build.Result != null)
                {
                    _bot.SendMessage(GetQaMessage(issue), responseChannel);
                    return TaskResult.Success;
                }
                else
                {
                    return TaskResult.NeedRetry;
                }
            }

            return TaskResult.Failure;
        }

        private static BotMessage GetNoCommitMessage(IssueInfo issue)
        {
            
            if (issue.Actor != null && issue.Status != null)
            {
                var text = string.Format("{0} changed <{1}|{2}> to {3}. NO COMMIT FOUND !!!", issue.Actor, issue.Link, issue.Key, issue.Status);
                return new BotMessage
                {
                    Attachments = new List<SlackAttachment>
                    {
                        new SlackAttachment
                        {
                            PreText = text,
                            Fallback = text,
                            Title = issue.Description,
                            ColorHex = "205081",
                            TitleLink = issue.Link
                        }
                    }
                };
            }
            else
            {
                var text = string.Format("<{0}|{1}>: NO COMMIT FOUND !!!",issue.Link, issue.Key);
                return new BotMessage
                {
                    Attachments = new List<SlackAttachment>
                    {
                        new SlackAttachment
                        {
                            PreText = text,
                            Fallback = text,
                            Title = issue.Description,
                            ColorHex = "205081",
                            TitleLink = issue.Link
                        }
                    }
                };
            }
        }

        private BotMessage GetQaMessage(IssueInfo issue)
        {
            if (issue.Actor != null && issue.Status != null)
            {
                var text = string.Format("{0} changed <{1}|{2}> to {3}. Change is deployed on TEST", issue.Actor, issue.Link, issue.Key, issue.Status);
                return new BotMessage()
                {
                    Attachments = new List<SlackAttachment>()
                    {
                        new SlackAttachment()
                        {
                            //<https://72media.atlassian.net/browse/RR-1463|RR-1463>
                            PreText = text,
                            Fallback = text,
                            Title = issue.Description,
                            ColorHex = "205081",
                            TitleLink = issue.Link
                        }
                    }
                };
            }
            else
            {
                var text = string.Format("<{0}|{1}>: Change is deployed on TEST", issue.Link, issue.Key);
                return new BotMessage
                {
                    Text = text
                };
            }
        }
    }
}
