﻿using MargieBot.Models;
using MargieBot.Responders;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SlackBot.Responders
{
    class CalculatorResponder : IResponder
    {
        public bool CanRespond(ResponseContext context)
        {
            return context.Message.Text.Split(' ').First().ToLower() == "calc";
        }

        public BotMessage GetResponse(ResponseContext context)
        {
            var calculation = context.Message.Text.Substring(4);
            return new BotMessage()
            {
                Text = Compute(calculation)
            };
        }

        private static string Compute(string computeString)
        {
            DataTable dt = new DataTable();
            string result;
            try
            {
                result = dt.Compute(computeString, "").ToString();
            } catch (Exception ex)
            {
                result = "Could not do that";
            }
            return result;           
        }
    }
}
