﻿namespace SlackBot.Responders
{
    using System.Collections.Generic;
    using System.Linq;
    using MargieBot;
    using MargieBot.Models;
    using MargieBot.Responders;

    public class OwnMessageForwarder : IResponder
    {
        private readonly Bot _bot;
        private readonly List<string> _controllingUsers;
        private const string MESSAGE_FORMAT = "{0} wrote: {1}";

        public OwnMessageForwarder(Bot bot, List<string> controllingUsers)
        {
            _bot = bot;
            _controllingUsers = controllingUsers;
        }

        public bool CanRespond(ResponseContext context)
        {
            var userName = context.UserNameCache[context.Message.User.ID];
            return !_controllingUsers.Contains(userName);
        }

        public BotMessage GetResponse(ResponseContext context)
        {
            if (!string.IsNullOrWhiteSpace(context.Message.Text))
            {
                foreach (var allowedUserId in _controllingUsers)
                {
                    bool isTargetAChannel = _bot.ConnectedChannels.Any(x => x.Name == "#" + allowedUserId);
                    _bot.Say(new BotMessage()
                    {
                        Text = string.Format(MESSAGE_FORMAT,context.Message.User.FormattedUserID, context.Message.Text),
                        ChatHub =
                            new SlackChatHub
                            {
                                ID = (isTargetAChannel ? "#" : "@") + allowedUserId,
                                Type = isTargetAChannel ? SlackChatHubType.Channel : SlackChatHubType.DM
                            }
                    });
                   
                }
            }

            return null;
        }
    }
}
