﻿namespace SlackBot.Responders
{
    using System.Collections.Generic;
    using System.Linq;
    using System.Text.RegularExpressions;
    using MargieBot;
    using MargieBot.Models;
    using MargieBot.Responders;
    using RestSharp.Extensions.MonoHttp;

    public class MessageForwarder : IResponder
    {
        private readonly Bot _bot;
        private readonly List<string> _allowedUserIds;
        private string COMMAND_REGEX = "send (?<text>.+) (?<targetUser>.+)";

        public MessageForwarder(Bot bot, List<string> allowedUserIds)
        {
            _bot = bot;
            _allowedUserIds = allowedUserIds;
        }

        public bool CanRespond(ResponseContext context)
        {
            var userName = context.UserNameCache[context.Message.User.ID];
            bool matchesCommandPattern = Regex.IsMatch(context.Message.Text, COMMAND_REGEX);
            return _allowedUserIds.Contains(userName) && matchesCommandPattern;
        }

        public BotMessage GetResponse(ResponseContext context)
        {
            var matches = Regex.Match(HttpUtility.HtmlDecode(context.Message.Text), COMMAND_REGEX);
            var textToSend = matches.Groups["text"];
            var targetUser = matches.Groups["targetUser"];
            if (textToSend != null && targetUser != null &&
                !string.IsNullOrWhiteSpace(textToSend.Value) && !string.IsNullOrWhiteSpace(targetUser.Value))
            {
                bool isTargetAChannel = _bot.ConnectedChannels.Any(x => x.Name == "#" + targetUser.Value);
                return new BotMessage()
                {
                    Text =  textToSend.Value,
                    ChatHub = new SlackChatHub() { ID = (isTargetAChannel? "#" : "@") + targetUser.Value, Type = isTargetAChannel ? SlackChatHubType.Channel : SlackChatHubType.DM }
                };
            }
            return new BotMessage()
            {
                Text = "Invalid command",
                ChatHub = new SlackChatHub() { ID = context.Message.User.ID, Type = SlackChatHubType.DM }
            };
            
           
        }
    }
}
