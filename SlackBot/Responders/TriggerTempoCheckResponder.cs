﻿namespace SlackBot.Responders
{
    using System.Collections.Generic;
    using System.Text.RegularExpressions;
    using MargieBot.Models;
    using MargieBot.Responders;
    using RestSharp.Extensions.MonoHttp;

    class TriggerTempoCheckResponder : IResponder
    {
        private List<string> _controllingUsers;
        private const string TriggerMessageText = "TriggerCheck";

        public TriggerTempoCheckResponder(List<string> controllingUsers)
        {
            _controllingUsers = controllingUsers;
        }
        public bool CanRespond(ResponseContext context)
        {
            var userName = context.UserNameCache[context.Message.User.ID];
            return _controllingUsers.Contains(userName) && context.Message.Text.StartsWith(TriggerMessageText);
        }

        public BotMessage GetResponse(ResponseContext context)
        {
            try
            {
                var interval = int.Parse(context.Message.Text.Split(' ')[1]);
                QuartzScheduler.Instance.OneTimeWorkLogReminder(interval);
                return new BotMessage()
                {
                    Text = "Hello "
                };
            }
            catch
            {
                return new BotMessage()
                {
                    Text = "Failed"
                };
            }
         
           
        }
    }
}
