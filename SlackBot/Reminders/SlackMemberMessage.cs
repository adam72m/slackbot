namespace SlackBot.Reminders
{
    public class SlackMemberMessage
    {
        public string Name { get; set; }
        public int Seconds { get; set; }
        public string AllowedPeriod { get; set; }
    }
}