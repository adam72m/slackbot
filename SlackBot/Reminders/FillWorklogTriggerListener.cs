namespace SlackBot.Reminders
{
    using System;
    using System.Collections.Generic;
    using JetBrains.Annotations;
    using Quartz;

    public class FillWorklogTriggerListener : ITriggerListener
    {
        private List<DayOfWeek> _skipDays;
        public FillWorklogTriggerListener([NotNull] List<DayOfWeek> skipDays)
        {
            if (skipDays == null) throw new ArgumentNullException(nameof(skipDays));
            _skipDays = skipDays;
        }

        public void TriggerFired(ITrigger trigger, IJobExecutionContext context)
        {
        }

        public bool VetoJobExecution(ITrigger trigger, IJobExecutionContext context)
        {
            if (_skipDays.Contains(DateTimeOffset.Now.DayOfWeek))
            {
                return true;
            }
            return false;
        }

        public void TriggerMisfired(ITrigger trigger)
        {
        }

        public void TriggerComplete(ITrigger trigger, IJobExecutionContext context, SchedulerInstruction triggerInstructionCode)
        {
        }

        public string Name
        {
            get { return "FillWorklogTriggerListener"; } }
    }
}