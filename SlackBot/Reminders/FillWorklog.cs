﻿namespace SlackBot.Reminders
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using Newtonsoft.Json;
    using Quartz;
    using Slackbot.Shared.Logging;
    using TechTalk.JiraRestClient.Tempo;
    using FluentDateTime;

    public class FillWorklog : IJob
    {
        public static string ResultKey = "resultkey";
        private int _allowedDaysBehind;

        private Log4NetLogger log;

        public FillWorklog()
        {
            log = new Log4NetLogger(typeof(FillWorklog));
        }

        public void Execute(IJobExecutionContext context)
        {
            try
            {
                var jobDataMap = context.MergedJobDataMap;
                string jiraHost = jobDataMap.Get("JiraHost").ToString();
                string jiraUser = jobDataMap.Get("JiraUser").ToString();
                string jiraPassword = jobDataMap.Get("JiraPass").ToString();
                _allowedDaysBehind = Int32.Parse(jobDataMap.Get("AllowedDays").ToString());
                var userToSlackMapping =
                    JsonConvert.DeserializeObject<Dictionary<string, string>>(
                        jobDataMap.Get("UsersToSlackMapping").ToString());

                var tempoClient = new TempoClient(jiraHost, jiraUser, jiraPassword);

                var teams = tempoClient.GetTeams();
                List<SlackMemberMessage> slackMembersToNotify = new List<SlackMemberMessage>();

                DateTime dateFrom = DateTime.Now.SubtractBusinessDays(_allowedDaysBehind);
                foreach (var team in teams)
                {
                    var members = tempoClient.GetTeamMembers(team.id.ToString());
                    var membersToCheck = members
                        .Where(x => userToSlackMapping.ContainsKey(x.member.name))
                        .Where(x => x.member.activeInJira && x.membership.status == "active");
                    log.Log(LogLevel.Info,
                        () =>
                            string.Format("Users: {0}",
                                string.Join(", ", userToSlackMapping.Select(x => $"{x.Key} : {x.Value}"))));

                    foreach (var teamMember in membersToCheck)
                    {
                        var workLogs = tempoClient.GetWorklogs(teamMember.member.name, dateFrom);
                        var timeSpent = workLogs.Sum(x => x.timeSpentSeconds);
                        if (timeSpent <= _allowedDaysBehind*TimeSpan.FromHours(4).TotalSeconds)
                        {
                            log.Log(LogLevel.Info, () => string.Format("{0} no work!", teamMember.member.name));
                            slackMembersToNotify.Add(
                                new SlackMemberMessage
                                {
                                    Name = userToSlackMapping[teamMember.member.name],
                                    AllowedPeriod = $"{_allowedDaysBehind} days",
                                    Seconds = timeSpent
                                });
                        }
                    }
                }

                if (slackMembersToNotify.Any())
                {
                    jobDataMap[ResultKey] = JsonConvert.SerializeObject(slackMembersToNotify);
                }
            }
            catch (Exception ex)
            {
                log.Log(LogLevel.Error, () => $"Error executing FillWorklog job: {ex}");
            }
        }
    }
}
