using System;
using System.Collections.Generic;
using System.Text;
using Quartz;
using SlackBot;

namespace SlackBot.Reminders
{
    using System.Linq;
    using System.Text;
    using Common.Logging;
    using MargieBot.Models;
    using Newtonsoft.Json;

    public class FillWorklogJobListener : IJobListener
    {
        private MargieBot.Bot _bot;
        private ILog log = LogManager.GetLogger<FillWorklogJobListener>();

        public FillWorklogJobListener(MargieBot.Bot bot)
        {
            _bot = bot;
        }

        public void JobToBeExecuted(IJobExecutionContext context)
        {
            log.Debug("JobToBeExecuted");
        }

        public void JobExecutionVetoed(IJobExecutionContext context)
        {
            log.Debug("JobExecutionVetoed");
        }

        public void JobWasExecuted(IJobExecutionContext context, JobExecutionException jobException)
        {
            log.Debug("JobWasExecuted");
            try
            {
                var result = context.MergedJobDataMap[FillWorklog.ResultKey];
                if (result != null)
                {
                    var usersToNotify = JsonConvert.DeserializeObject<List<SlackMemberMessage>>(result.ToString());
                    log.Info($"usersToNotify: {result}");
                    foreach (var user in usersToNotify)
                    {
                        try
                        {
                            SendReminder(user);
                        }
                        catch(Exception ex)
                        {
                            log.Error($"Failed sending a reminder: {ex}");
                        }
                    }

                }
            }
            catch (Exception ex)
            {
                log.Error($"Error executing FillWorklog job: {ex}");
            }
        }

        private void SendReminder(SlackMemberMessage user)
        {
            log.Debug("Sending reminder");
            _bot.Say(new BotMessage()
            {
                Text = $"You only filled in {(float)user.Seconds/3600} hours in Tempo since {user.AllowedPeriod}. Please fill your hours.",
                Attachments = new List<SlackAttachment>
                    {
                        new SlackAttachment
                        {
                            Title = "==> your Tempo Timesheets <==",
                            TitleLink = "https://72media.atlassian.net/plugins/servlet/ac/is.origo.jira.tempo-plugin/tempo-my-work"
                        }
                    },
                ChatHub =
                    new SlackChatHub
                    {
                        ID = $"@{user.Name}",
                        Type = SlackChatHubType.DM
                    }
            });
        }

        public string Name
        {
            get { return "FillWorklogJobListener"; }
        }
    }
}