﻿namespace SlackBot
{
    using System;

    public class IssueInfo
    {
        public string Actor { get; set; }
        public string Key { get; set; }
        public string Type { get; set; }
        public string Description { get; set; }
        public string Status { get; set; }
        public string Link { get { return String.Format("https://72media.atlassian.net/browse/{0}", Key); } }
        public override string ToString()
        {
            return Key;
        }
    }
}
