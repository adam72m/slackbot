﻿namespace SlackBot
{
    using System.Linq;
    using MargieBot;
    using MargieBot.Models;

    public static class BotExtensions
    {
        public static void SendMessage(this Bot bot, string message, string channel)
        {
            var qaAnnouncement = new BotMessage()
            {
                Text = message,
                ChatHub = GetChatHub(bot, channel)
            };
            bot.Say(qaAnnouncement);
        }

        public static void SendMessage(this Bot bot, BotMessage message, string channel)
        {
            message.ChatHub = GetChatHub(bot, channel); 
            bot.Say(message);
        }

        private static SlackChatHub GetChatHub(Bot bot, string channel)
        {
            bool isTargetAChannel = bot.ConnectedChannels.Any(x => x.Name == "#" + channel);
            var chatHub = new SlackChatHub()
            {
                ID = (isTargetAChannel ? "#" : "@") + channel,
                Type = isTargetAChannel ? SlackChatHubType.Channel : SlackChatHubType.DM
            };
            return chatHub;
        }
    }
}
