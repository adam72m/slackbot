﻿namespace SlackBot
{
    using System;
    using System.Collections.Generic;
    using System.Configuration;
    using System.Diagnostics;
    using System.Text.RegularExpressions;
    using System.Threading;
    using System.Threading.Tasks;
    using BitbucketIntegration;
    using MargieBot;
    using MargieBot.Models;
    using Newtonsoft.Json;
    using Responders;
    using Slackbot.Shared;
    using Slackbot.Shared.Logging;
    using TeamCityIntegration;
    using TechTalk.JiraRestClient;

    public class MargieBotImplementation
    {
        private readonly string _token;
        private readonly IBitBucketRepository _bitBucketRepository;
        private readonly TeamCityConfiguration _configuration;
        private readonly JiraConfiguration _jiraConfiguration;
        private string JIRA_BOT_ID = "B1LGEBK6F";
        IssueToQADeployed _qaResponder;
        private ILog _log = new Log4NetLogger("");

        public MargieBotImplementation(string token, IBitBucketRepository bitBucketRepository, TeamCityConfiguration configuration, JiraConfiguration jiraConfiguration)
        {
            _token = token;
            _bitBucketRepository = bitBucketRepository;
            _configuration = configuration;
            _jiraConfiguration = jiraConfiguration;
        }

        public async Task Run(Bot bot)
        {
            await bot.Connect(_token);
            await bot.Say(new BotMessage()
            {
                ChatHub = new SlackChatHub() {ID = "@adam", Name = "adam", Type = SlackChatHubType.DM},
                Text = "I'm online"
            });
            var forwardingResponder = new MessageForwarder(bot,
                new List<string>() {"adam"});
            var ownMessageForwardingResponder = new OwnMessageForwarder(bot,
                new List<string>() {"adam"});
            var joseTestResponder = new JoseTestResponder();
            var calcResponder = new CalculatorResponder();
            bot.Responders.Add(calcResponder);
            bot.Responders.Add(joseTestResponder);
            bot.Responders.Add(forwardingResponder);
            bot.Responders.Add(ownMessageForwardingResponder);
            bot.Responders.Add(new TriggerTempoCheckResponder(new List<string>() { "adam" }));
            bot.MessageReceived += Bot_MessageReceived;
            
            _qaResponder = new IssueToQADeployed(_configuration, _bitBucketRepository, "develop",
                "RisiGo_RisiGoTestBuild", "general", 50, TimeSpan.FromSeconds(60), _log, bot);
            bot.Responders.Add(_qaResponder);
           //var str= @"{""text"":"""",""bot_id"":""B1LGEBK6F"",""attachments"":[{""fallback"":""Adam changed Bug <https://72media.atlassian.net/browse/RR-1364|RR-1481> from \""Impeded\"" to \""Registered\"""",""pretext"":""Adam changed Bug <https://72media.atlassian.net/browse/RR-1364|RR-1483> from \""Impeded\"" to \""Registered\"""",""title"":""RISGO-573 When adding klantgegevens online and other klantgegevens on iPad after a sync the change is synced as a block "",""id"":1,""title_link"":""https://72media.atlassian.net/browse/RR-1463"",""color"":""205081"",""fields"":[{""title"":""Creator"",""value"":""Adam"",""short"":true}]}],""type"":""message"",""subtype"":""bot_message"",""team"":""T02570ZNY"",""user_team"":""T02570ZNY"",""channel"":""D1LGRQE30"",""ts"":""1467053041.000023""}";
           // Bot_MessageReceived(str);
        }

        private void Bot_MessageReceived(string messageText)
        {
            try
            {
                var msg = JsonConvert.DeserializeObject<MessageFromBot>(messageText);
                if (msg.bot_id == JIRA_BOT_ID)
                {
                    var regex =
                        @"(?'actor'[\w\s]+) changed (?<issueType>[\w-]+) (?<link><[^|]+[|])(?<key>RR-[\d]+)> from (?<statusFrom>[""].+[""]) to (?<statusTo>[""].+[""])";
                    var match = Regex.Match(msg.Attachments[0].PreText, regex);
                    var issue = match.Groups["key"].Value;
                    var actor = match.Groups["actor"].Value;
                    var type = match.Groups["issueType"].Value;
                    var status = match.Groups["statusTo"].Value;
                    var name = msg.Attachments[0].Title;
                    var issueInfo = new IssueInfo()
                    {
                        Key = issue,
                        Status = status,
                        Actor = actor,
                        Description = name,
                        Type = type
                    };
                    _qaResponder.HandleIssue(issueInfo, "test-risigo");
                }
            }
            catch (Exception ex)
            {
                _log.Log(LogLevel.Debug, ()=> string.Format("Error receiving message: {0}", ex.ToString()));
            }
        }

        class MessageFromBot : BotMessage
        {
            public string bot_id { get; set; }
        }
    }
}
