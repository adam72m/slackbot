namespace BitbucketIntegration
{
    using Slackbot.Shared.Models;

    public interface IBitBucketRepository
    {
        Commit GetLastCommitForJiraIssue(string issueKey, string branch);
    }
}