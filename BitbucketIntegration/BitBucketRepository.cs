﻿namespace BitbucketIntegration
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text.RegularExpressions;
    using SharpBucket.V2;
    using SharpBucket.V2.EndPoints;
    using Slackbot.Shared;
    using Slackbot.Shared.Models;

    public class BitBucketRepository : IBitBucketRepository
    {
        private readonly string _account;
        private readonly string _repository;
        SharpBucketV2 _sharpBucket;
        RepositoriesEndPoint _repositoriesEndPoint;
        const int MAX_COMMITS_TO_CHECK =500;
        public BitBucketRepository(string key, string secret, string account, string repository)
        {
            _account = account;
            _repository = repository;
            _sharpBucket = new SharpBucketV2();
            _sharpBucket.OAuth2LeggedAuthentication(key, secret);
            _repositoriesEndPoint = _sharpBucket.RepositoriesEndPoint();
        }

        public Commit GetLastCommitForJiraIssue(string issueKey, string branch)
        {
            var commits = _repositoriesEndPoint.RepositoryResource(_account, _repository).ListCommits(branch, max: MAX_COMMITS_TO_CHECK);
            return commits.OrderByDescending(x => x.date)
                .Where(x => Regex.IsMatch(x.message,string.Format(@"{0}\b",issueKey)))
                .Select(x => new Commit()
                {
                    Message = x.message,
                    Author = x.author.raw,
                    Date = DateTimeOffset.Parse( x.date),
                    Hash = x.hash
                })
                .FirstOrDefault();
        }

    }
}
