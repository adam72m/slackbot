﻿namespace BitbucketIntegration
{
    using System.Dynamic;

    public class BitbucketConfiguration
    {
        public string BbucketKey { get; set; }
        public string BbucketSecret { get; set; }
        public string RepositoryName { get; set; }
        public string AccountName { get; set; }
    }
}