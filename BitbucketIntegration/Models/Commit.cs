﻿namespace Slackbot.Shared.Models
{
    using System;

    public class Commit
    {
        public DateTimeOffset Date { get; set; }
        public string Author { get; set; }
        public string Message { get; set; }
        public string Hash { get; set; }
    }
}
