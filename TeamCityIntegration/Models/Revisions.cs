namespace TeamCityIntegration
{
    using System.Collections.Generic;

    public class Revisions
    {
        public int count { get; set; }
        public List<Revision> revision { get; set; }
    }
}