namespace TeamCityIntegration
{
    public class Triggered
    {
        public string type { get; set; }
        public string details { get; set; }
        public string date { get; set; }
    }
}