namespace TeamCityIntegration
{
    using System.Collections.Generic;

    public class ChangeList
    {
        public int count { get; set; }
        public string href { get; set; }

        public List<Change> change { get; set; } = new List<Change>();
    }
}