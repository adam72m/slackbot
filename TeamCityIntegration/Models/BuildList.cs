namespace TeamCityIntegration
{
    using System.Collections.Generic;

    public class BuildList
    {
        public int count { get; set; }
        public string href { get; set; }
        public string nextHref { get; set; }

        public List<Build> build { get; set; } = new List<Build>();
    }
}