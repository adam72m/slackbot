namespace TeamCityIntegration
{
    public class BuildDetails
    {
        public int id { get; set; }
        public string buildTypeId { get; set; }
        public string number { get; set; }
        public string status { get; set; }
        public string state { get; set; }
        public string href { get; set; }
        public string webUrl { get; set; }
        public string statusText { get; set; }
        public BuildType buildType { get; set; }
        public string queuedDate { get; set; }
        public string startDate { get; set; }
        public string finishDate { get; set; }
        public Triggered triggered { get; set; }
        public LastChanges lastChanges { get; set; }
        public Changes changes { get; set; }
        public Revisions revisions { get; set; }
        public Agent agent { get; set; }
        public Artifacts artifacts { get; set; }
        public RelatedIssues relatedIssues { get; set; }
        public Statistics statistics { get; set; }
        [RestSharp.Deserializers.DeserializeAs(Name = "artifact-dependencies")]
        [RestSharp.Serializers.SerializeAs(Name = "artifact-dependencies")]
        public ArtifactDependencies artifact_dependencies { get; set; }

    }
}