namespace TeamCityIntegration
{
    using RestSharp.Deserializers;
    using RestSharp.Serializers;

    public class VcsRootInstance
    {
        public string id { get; set; }

        [DeserializeAs(Name = "vcs-root-id")]
        [SerializeAs(Name = "vcs-root-id")]
        public string vcs_root_id { get; set; }

        public string name { get; set; }
        public string href { get; set; }
    }
}