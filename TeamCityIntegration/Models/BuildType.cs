namespace TeamCityIntegration
{
    public class BuildType
    {
        public string id { get; set; }
        public string name { get; set; }
        public string projectName { get; set; }
        public string projectId { get; set; }
        public string href { get; set; }
        public string webUrl { get; set; }
    }
}