namespace TeamCityIntegration
{
    using System.Collections.Generic;

    public class ArtifactDependencies
    {
        public int count { get; set; }
        public List<Build> build { get; set; }
    }
}