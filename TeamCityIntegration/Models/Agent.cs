namespace TeamCityIntegration
{
    public class Agent
    {
        public int id { get; set; }
        public string name { get; set; }
        public int typeId { get; set; }
        public string href { get; set; }
    }
}