namespace TeamCityIntegration
{
    using RestSharp.Deserializers;
    using RestSharp.Serializers;

    public class Revision
    {
        public string version { get; set; }

        [DeserializeAs(Name = "vcs-root-instance")]
        [SerializeAs(Name = "vcs-root-instance")]
        public VcsRootInstance vcs_root_instance { get; set; }
    }
}