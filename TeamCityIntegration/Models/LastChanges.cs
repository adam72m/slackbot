namespace TeamCityIntegration
{
    using System.Collections.Generic;

    public class LastChanges
    {
        public int count { get; set; }
        public List<Change> change { get; set; }
    }
}