namespace TeamCityIntegration
{
    public class TeamCityConfiguration
    {
        public bool UseSSL { get; set; }
        public bool ActAsGuest { get; set; }
        public object HostName { get; set; }
        public string DefaultVcsRoot { get; set; }
        public string UserName { get; set; }
        public string Password { get; set; }
    }
}