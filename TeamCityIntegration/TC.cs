﻿namespace TeamCityIntegration
{
    using System.Linq;
    using System.Text.RegularExpressions;
    using System.Threading.Tasks;
    using RestSharp;
    using RestSharp.Authenticators;

    public class TC
    {
        private readonly TeamCityConfiguration _configuration;
        private readonly RestClient _restClient;

        public TC(TeamCityConfiguration configuration)
        {
            _configuration = configuration;
            _restClient = new RestClient(CreateUrl(""));
            var auth = new HttpBasicAuthenticator(_configuration.UserName, _configuration.Password);
            _restClient.Authenticator = auth;
        }

        public string GetBuildByBuildTypeId(string buildTypeId)
        {
            var request = new RestRequest("/app/rest/builds/?locator=buildType:{id},start:0,count:1", Method.GET);
            request.AddUrlSegment("id", buildTypeId);

            return _restClient.Execute(request).Content;
        }

        public async Task<string> GetBuildNumberWithChange(string changeHash, string buildType)
        {
            var buildRequest =
                new RestRequest("/app/rest/builds?locator=buildType:{buildTypeId},status:SUCCESS,count:500");
            buildRequest.AddUrlSegment("buildTypeId", buildType);
            var buildList = await _restClient.ExecuteGetTaskAsync<BuildList>(buildRequest);
            foreach (var build in buildList.Data.build)
            {
                var buildDetailRequest = new RestRequest("/app/rest/changes?locator=build:(id:{buildId})");
                buildDetailRequest.AddUrlSegment("buildId", build.id.ToString());
                var buildDetail = await _restClient.ExecuteGetTaskAsync<ChangeList>(buildDetailRequest);
                if (buildDetail.Data.change.Any(x => x.version == changeHash))
                {
                    return build.number;
                }
            }
            return null;
        }

        public async Task<bool> CheckBuildNumberDeployed(string buildNumber, string deployBuildType, string searchedBuildType)
        {
            var buildRequest =
               new RestRequest("/app/rest/builds?locator=buildType:{buildTypeId},status:SUCCESS,count:500");
            buildRequest.AddUrlSegment("buildTypeId", deployBuildType);
            var buildList = await _restClient.ExecuteGetTaskAsync<BuildList>(buildRequest);
            foreach (var build in buildList.Data.build)
            {
                var buildDetailRequest = new RestRequest("/app/rest/builds/id:{buildId}");
                buildDetailRequest.AddUrlSegment("buildId", build.id.ToString());
                var buildDetail = await _restClient.ExecuteGetTaskAsync<BuildDetails>(buildDetailRequest);
                if (buildDetail.Data.artifact_dependencies.build.Any(x => GetBuildMinorNumber(x.number) >= GetBuildMinorNumber(buildNumber) && x.buildTypeId == searchedBuildType))
                {
                    return true;
                }
            }
            return false;
        }

        private static int GetBuildMinorNumber(string buildNumber)
        {
            string buildNumberRegex = @"[\d]+.[\d]+.(?<minor>[\d]+)";
            string buildMinorNumber = Regex.Match(buildNumber, buildNumberRegex).Groups["minor"].Value;
            return int.Parse(buildMinorNumber);
        }

        private string CreateUrl(string urlPart)
        {
            var protocol = _configuration.UseSSL ? "https://" : "http://";
            var authType = _configuration.ActAsGuest ? "/guestAuth" : "/httpAuth";

            return string.Format("{0}{1}{2}{3}", protocol, _configuration.HostName, authType, urlPart);
        }

        public void TryTempo()
        {
            var restClient = new RestClient("http://s-build.72media.nl:8080");
            var auth = new HttpBasicAuthenticator(_configuration.UserName, _configuration.Password);
            restClient.Authenticator = auth;
            var request = new RestRequest("/rest/tempo-timesheets/3/worklogs/", Method.GET);

            var a = restClient.Execute(request).Content;
            ///rest/tempo-timesheets/3/worklogs/
        }
    }
}
