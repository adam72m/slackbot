﻿namespace SlackBotConsole
{
    using System;
    using System.Collections.Generic;
    using System.Configuration;
    using System.Diagnostics;
    using System.IO;
    using System.Linq;
    using System.Threading.Tasks;
    using BitbucketIntegration;
    using log4net.Config;
    using MargieBot;
    using Newtonsoft.Json;
    using SlackBot;
    using SlackBotService;
    using TeamCityIntegration;
    using TechTalk.JiraRestClient;
    using TechTalk.JiraRestClient.Tempo;

    class Program
    {
        
        static void Main(string[] args)
        {
            var bbucketConfiguration = JsonConvert.DeserializeObject<BitbucketConfiguration>(File.ReadAllText("bbucketConfig.json"));
            var botToken = JsonConvert.DeserializeObject<string>(File.ReadAllText("botToken.json"));
            var tcConfiguration = JsonConvert.DeserializeObject<TeamCityConfiguration>(File.ReadAllText("tcConfig.json"));
            var bitBucketRepository = new BitBucketRepository(bbucketConfiguration.BbucketKey,
                bbucketConfiguration.BbucketSecret, bbucketConfiguration.AccountName, bbucketConfiguration.RepositoryName);
            XmlConfigurator.Configure(new FileInfo("log4net.config"));

            JiraConfiguration jiraConfiguration = null;
            if (File.Exists("jiraConfig.json"))
            {
                jiraConfiguration = JsonConvert.DeserializeObject<JiraConfiguration>(File.ReadAllText("jiraConfig.json"));
            }
            var bot = new MargieBotImplementation(botToken, bitBucketRepository, tcConfiguration, jiraConfiguration);
            var margieBot = new Bot();
            var botTask = bot.Run(margieBot);

            string usersToSlackMapping = File.ReadAllText("usersToSlackMapping.json");
            //var task = bot.Run();
            var q =  QuartzScheduler.Instance;
            q.Init(jiraConfiguration, "Medewerkers", usersToSlackMapping, margieBot, hourInterval: 24, allowedDays: 4);
            q.StartFillWorklogReminder();
            botTask.Wait();
            Console.ReadLine();
        }
    }
}
