﻿namespace TechTalk.JiraRestClient
{
    public class JiraConfiguration
    {
        public string JiraHost { get; set; }
        public string Username { get; set; }
        public string Password { get; set; }
    }
}
