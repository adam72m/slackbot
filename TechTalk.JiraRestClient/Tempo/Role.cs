namespace TechTalk.JiraRestClient.Tempo
{
    public class Role
    {
        public int id { get; set; }
        public string name { get; set; }
        public bool Property_default { get; set; }
    }
}