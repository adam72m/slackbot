﻿namespace TechTalk.JiraRestClient.Tempo
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Net;
    using System.Text;
    using RestSharp;
    using RestSharp.Deserializers;

    public class TempoClient 
    {
        private readonly string _username;
        private readonly string _password;
        private string _baseApiUrl;
        private JsonDeserializer _deserializer;
        private RestClient _client;

        public TempoClient(string baseUrl, string username, string password)
        {
            this._username = username;
            this._password = password;
            
            _baseApiUrl = new Uri(new Uri(baseUrl), "/rest/").ToString();
            _client = new RestClient(_baseApiUrl);
            _deserializer = new JsonDeserializer();
        }

        private RestRequest CreateRequest(Method method, String path)
        {
            var request = new RestRequest { Method = method, Resource = path, RequestFormat = DataFormat.Json };
            request.AddParameter("tempoApiToken", "81b49ff6-8299-4d58-a952-3583d965e101");
            request.AddHeader("Authorization", "Basic " + Convert.ToBase64String(Encoding.UTF8.GetBytes(String.Format("{0}:{1}", _username, _password))));
            return request;
        }

        private void AssertStatus(IRestResponse response, HttpStatusCode status)
        {
            if (response.ErrorException != null)
                throw new JiraClientException("Transport level error: " + response.ErrorMessage, response.ErrorException);
            if (response.StatusCode != status)
                throw new JiraClientException("JIRA returned wrong status: " + response.StatusDescription, response.Content);
        }

        public IEnumerable<Worklog> GetWorklogs(string username, DateTime dateFrom)
        {
            var path = string.Format("tempo-timesheets/3/worklogs/?username={0}&dateFrom={1}", username, dateFrom.ToString("yyyy-MM-dd"));
            var request = CreateRequest(Method.GET, path);
            var response = _client.Execute<List<Worklog>>(request);
            AssertStatus(response, HttpStatusCode.OK);
            return response.Data;
        }

        public IEnumerable<Team> GetTeams()
        {
            var path = string.Format("tempo-teams/1/team");
            var request = CreateRequest(Method.GET, path);
            request.AddParameter("permissionKey", "tempo.teams.browse.team");
            var response = _client.Execute<List<Team>>(request);
            AssertStatus(response, HttpStatusCode.OK);
            return response.Data;
        }

        public IEnumerable<TeamMember> GetTeamMembers(string teamId)
        {
            var path = string.Format("tempo-teams/2/team/{0}/member", teamId);
            var request = CreateRequest(Method.GET, path);
            var response = _client.Execute<List<TeamMember>>(request);
            AssertStatus(response, HttpStatusCode.OK);
            return response.Data;
        }
    }
}
