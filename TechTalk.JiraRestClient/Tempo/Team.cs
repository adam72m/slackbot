﻿namespace TechTalk.JiraRestClient.Tempo
{
    public class Team
    {
        public int id { get; set; }
        public string name { get; set; }
        public string summary { get; set; }
        public string lead { get; set; }
        public string program { get; set; }
        public string mission { get; set; }
    }

}
