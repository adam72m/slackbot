namespace TechTalk.JiraRestClient.Tempo
{
    public class MemberBean
    {
        public int teamMemberId { get; set; }
        public string name { get; set; }
        public string type { get; set; }
        public Avatar avatar { get; set; }
        public bool activeInJira { get; set; }
        public string displayname { get; set; }
    }
}