﻿namespace TechTalk.JiraRestClient.Tempo
{
    public class TeamProgram
    {
        public int id { get; set; }
        public string name { get; set; }
        public Manager manager { get; set; }
    }
}