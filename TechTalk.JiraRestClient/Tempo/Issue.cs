﻿namespace TechTalk.JiraRestClient.Tempo
{
    public class Issue
    {
        public string self { get; set; }
        public int id { get; set; }
        public int projectId { get; set; }
        public string key { get; set; }
        public int remainingEstimateSeconds { get; set; }
        public IssueType issueType { get; set; }
        public string summary { get; set; }
    }
}