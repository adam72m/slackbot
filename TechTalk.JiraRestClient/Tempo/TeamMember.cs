﻿namespace TechTalk.JiraRestClient.Tempo
{
    public class TeamMember
    {
        public int id { get; set; }
        public Member member { get; set; }
        public MemberBean memberBean { get; set; }
        public Membership membership { get; set; }
        public MembershipBean membershipBean { get; set; }
        public bool showDeactivate { get; set; }

        public override string ToString()
        {
            return $"Name: {member?.name} Active: {member?.activeInJira}";
        }
    }
}