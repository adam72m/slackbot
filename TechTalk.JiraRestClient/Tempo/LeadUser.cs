namespace TechTalk.JiraRestClient.Tempo
{
    public class LeadUser
    {
        public string name { get; set; }
        public string key { get; set; }
        public Avatar avatar { get; set; }
        public bool jiraUser { get; set; }
        public string displayname { get; set; }
    }
}