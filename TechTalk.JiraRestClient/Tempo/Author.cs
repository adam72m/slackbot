﻿namespace TechTalk.JiraRestClient.Tempo
{
    public class Author
    {
        public string self { get; set; }
        public string name { get; set; }
        public string displayName { get; set; }
        public string avatar { get; set; }
    }
}