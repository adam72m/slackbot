﻿namespace TechTalk.JiraRestClient.Tempo
{
    public class MembershipBean
    {
        public int id { get; set; }
        public Role role { get; set; }
        public string dateFrom { get; set; }
        public string dateTo { get; set; }
        public string dateFromANSI { get; set; }
        public string dateToANSI { get; set; }
        public string availability { get; set; }
        public int teamMemberId { get; set; }
        public int teamId { get; set; }
        public string status { get; set; }
    }
}