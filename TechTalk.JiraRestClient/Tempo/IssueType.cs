namespace TechTalk.JiraRestClient.Tempo
{
    public class IssueType
    {
        public string name { get; set; }
        public string iconUrl { get; set; }
    }
}