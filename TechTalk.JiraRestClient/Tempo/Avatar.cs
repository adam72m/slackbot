﻿namespace TechTalk.JiraRestClient.Tempo
{
    public class Avatar
    {
        [RestSharp.Deserializers.DeserializeAs(Name = "16x16")]
        [RestSharp.Serializers.SerializeAs(Name = "16x16")]
        public string Size16x16 { get; set; }
        [RestSharp.Deserializers.DeserializeAs(Name = "24x24")]
        [RestSharp.Serializers.SerializeAs(Name = "24x24")]
        public string Size24x24 { get; set; }
        [RestSharp.Deserializers.DeserializeAs(Name = "32x32")]
        [RestSharp.Serializers.SerializeAs(Name = "32x32")]
        public string Size32x32 { get; set; }
        [RestSharp.Deserializers.DeserializeAs(Name = "48x48")]
        [RestSharp.Serializers.SerializeAs(Name = "48x48")]
        public string Size48x48 { get; set; }
    }
}