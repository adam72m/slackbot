﻿namespace TechTalk.JiraRestClient.Tempo
{
    using System;
    using System.Collections.Generic;

    public class Worklog
    {
        public int timeSpentSeconds { get; set; }
        public DateTime dateStarted { get; set; }
        public string comment { get; set; }
        public string self { get; set; }
        public int id { get; set; }
        public Author author { get; set; }
        public Issue issue { get; set; }
        public List<object> worklogAttributes { get; set; }
    }
}
