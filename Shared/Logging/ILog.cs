﻿namespace Slackbot.Shared.Logging
{
    using System;

    public interface ILog
    {
        void Log(LogLevel level, Func<string> message);
    }
}
