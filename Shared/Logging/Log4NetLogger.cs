﻿namespace Slackbot.Shared.Logging
{
    using System;
    using log4net;

    public class Log4NetLogger : ILog
    {
        private log4net.ILog _log;
        public Log4NetLogger(Type loggerType):this(loggerType.GetType().FullName)
        {
        }

        public Log4NetLogger(string name)
        {
            _log = LogManager.GetLogger(name);
        }

        public void Log(LogLevel level, Func<string> message)
        {
            switch (level)
            {
                case LogLevel.Debug:
                    _log.Debug(message());
                    break;
                case LogLevel.Info:
                    _log.Info(message());
                    break;
                case LogLevel.Warn:
                    _log.Warn(message());
                    break;
                case LogLevel.Error:
                    _log.Error(message());
                    break;
                case LogLevel.Fatal:
                    _log.Fatal(message());
                    break;
                default:
                    throw new ArgumentOutOfRangeException(nameof(level), level, null);
            }
        }
    }
}
