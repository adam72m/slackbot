﻿namespace Slackbot.Shared.Logging
{
    using System;

    public class ConsoleLogger : ILog
    {
        public void Log(LogLevel level, Func<string> message)
        {
            Console.WriteLine("{0} {1}:{2}", level.ToString(), DateTime.Now.TimeOfDay, message());
        }
    }
}
