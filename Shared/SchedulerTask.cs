namespace Slackbot.Shared
{
    using System;
    using System.Runtime.Serialization;
    using JetBrains.Annotations;

    public class SchedulerTask
    {
        public Func<TaskResult> Action { get; private set; }
        public string Id { get; private set; }
        public int MaxRetryCount { get; set; }
        public int CurrentRetryCount { get; set; }
        public TimeSpan RetryInterval { get; set; }
        public Exception Exception { get; set; }

        public Action<SchedulerTask> PostNegativeResultAction { get; private set; }

        public SchedulerTask([NotNull]Func<TaskResult> action, Action<SchedulerTask> postNegativeResultAction, [NotNull]string id)
        {
            PostNegativeResultAction = postNegativeResultAction;
            Action = action;
            Id = id;
        }

        public SchedulerTask([NotNull]Func<TaskResult> action, [NotNull]string id)
        {
            Action = action;
            Id = id;
        }

        public override int GetHashCode()
        {
            return Id.GetHashCode();
        }

        public override bool Equals(object obj)
        {
            var otherInstance = obj as SchedulerTask;
            if (otherInstance == null)
                return false;
            return object.Equals(Id, otherInstance.Id);
        }
    }
}