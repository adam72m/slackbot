﻿namespace Slackbot.Shared
{
    using System;

    public interface IScheduler
    {
        void ScheduleTask(SchedulerTask schedulerTask);
        void ScheduleTask(SchedulerTask schedulerTask, TimeSpan startDelay);
    }
}
