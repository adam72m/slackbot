﻿namespace Slackbot.Shared
{
    using System;

    public class TaskAlreadyScheduledException : Exception
    {
        public TaskAlreadyScheduledException(string message) : base(message)
        {
        }
    }
}