﻿namespace Slackbot.Shared
{
    public enum TaskResult
    {
        Success,
        Failure,
        NeedRetry,
        RetryWithAction
    }
}