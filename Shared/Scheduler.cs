﻿namespace Slackbot.Shared
{
    using System;
    using System.Collections.Concurrent;
    using System.Threading.Tasks;
    using System.Timers;
    using Logging;

    public class Scheduler : IScheduler
    {
        
        private ILog _log;
        readonly ConcurrentDictionary<SchedulerTask, bool> _scheduledTasks = new ConcurrentDictionary<SchedulerTask, bool>();

        public Scheduler(ILog log)
        {
            _log = log;
        }

        public void ScheduleTask(SchedulerTask schedulerTask)
        {
            ScheduleTask(schedulerTask, TimeSpan.FromMilliseconds(1));
        }

        public void ScheduleTask(SchedulerTask schedulerTask, TimeSpan startDelay)
        {
            bool isTaskAlreadyScheduled = !_scheduledTasks.TryAdd(schedulerTask, true);
            if (isTaskAlreadyScheduled)
            {
                string errorMessage = String.Format("Task {0} is already scheduled", schedulerTask.Id);
                _log.Log(LogLevel.Error, ()=> errorMessage);
                schedulerTask.Exception= new TaskAlreadyScheduledException(errorMessage);
                throw schedulerTask.Exception;
            }

            var timer = new Timer()
            {
                AutoReset = false,
                Interval = startDelay.TotalMilliseconds + 1,
            };
            timer.Elapsed += (obj, arg) =>
            {
                 ExecuteTask(timer, schedulerTask);
            };
            timer.Start();
        }

        private void ExecuteTask(Timer timer, SchedulerTask scheduledTask)
        {
            var task = new Task<TaskResult>(scheduledTask.Action);
            
            task.ContinueWith((t2) =>
            {
                bool needToUnscheduleTask = true;
                if (t2.IsFaulted || t2.IsCanceled)
                {
                    needToUnscheduleTask = HandleTaskFailure(timer, scheduledTask, t2);
                }
                else
                {
                    switch (t2.Result)
                    {
                        case TaskResult.Success:
                            _log.Log(LogLevel.Debug, () => String.Format("Task {0} succeded", scheduledTask.Id));
                            break;
                        case TaskResult.Failure:
                        case TaskResult.NeedRetry:
                            needToUnscheduleTask = HandleTaskFailure(timer, scheduledTask, t2);
                            break;
                        case TaskResult.RetryWithAction:
                            if (scheduledTask.PostNegativeResultAction != null)
                            {
                                _log.Log(LogLevel.Debug, () => String.Format("Task {0}, Executing post negative result action", t2.Id));
                                try
                                {
                                    scheduledTask.PostNegativeResultAction(scheduledTask);
                                }
                                catch (Exception ex)
                                {
                                    _log.Log(LogLevel.Error, () => String.Format("Error executing post negative result action: {0}", ex));
                                }
                            }

                            needToUnscheduleTask = HandleTaskFailure(timer, scheduledTask, t2);
                            break;
                        default:
                            throw new ArgumentOutOfRangeException();
                    }
                }

                if (needToUnscheduleTask)
                {
                    RemoveFromSchedule(scheduledTask);
                }
            });
            _log.Log(LogLevel.Debug, () => String.Format("Task {0} starting", task.Id));
            task.Start();
        }

        private bool HandleTaskFailure(Timer timer, SchedulerTask scheduledTask, Task<TaskResult> t2)
        {
            bool needToRemoveTaskFromSchedule = false;
            if (scheduledTask.CurrentRetryCount++ < scheduledTask.MaxRetryCount)
            {
                _log.Log(LogLevel.Debug, () => String.Format("Task {0} retry nr {1}", t2.Id, scheduledTask.CurrentRetryCount));
                ScheduleRetry(timer, scheduledTask.RetryInterval, t2.Exception != null ? t2.Exception.Message : "No results");
            }
            else
            {
                _log.Log(LogLevel.Debug, () => String.Format("Task {0} not retrying, retry count: {1} exceeded", t2.Id, scheduledTask.MaxRetryCount));
                needToRemoveTaskFromSchedule = true;
            }

            return needToRemoveTaskFromSchedule;
        }

        private void RemoveFromSchedule(SchedulerTask scheduledTask)
        {
            bool removedTaskValue;
            bool successfulRemove = _scheduledTasks.TryRemove(scheduledTask, out removedTaskValue);
            if (successfulRemove)
            {
                _log.Log(LogLevel.Debug, () => String.Format("Task {0} removed from queue", scheduledTask.Id));
            }
            else
            {
                scheduledTask.Exception = new UnableToRemoveTaskFromScheduleException();
                throw scheduledTask.Exception;
            }
        }

        private void ScheduleRetry(Timer timer, TimeSpan retryInterval, string reason)
        {
            _log.Log(LogLevel.Debug, () => String.Format("Retry in {0} because: {1}", retryInterval, reason));
            timer.Interval = retryInterval.TotalMilliseconds;
            timer.Start();
        }
    }
}