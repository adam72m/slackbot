﻿namespace Slackbot.Shared
{
    using System;

    internal class UnableToRemoveTaskFromScheduleException : Exception
    {
    }
}