﻿namespace SlackBot.Service
{
    using System;
    using System.Configuration;
    using System.IO;
    using System.ServiceProcess;
    using System.Threading;
    using BitbucketIntegration;
    using log4net;
    using global::SlackBotService;
    using log4net.Config;
    using MargieBot;
    using Newtonsoft.Json;
    using TeamCityIntegration;
    using TechTalk.JiraRestClient;


    public partial class SlackBotService : ServiceBase
    {
        private MargieBotImplementation _bot;
        private ILog log = LogManager.GetLogger("SlackBotService");
        private ManualResetEvent _shutdownEvent = new ManualResetEvent(false);
        private Thread _thread;

        public SlackBotService()
        {
            InitializeComponent();
        }

        protected override void OnStart(string[] args)
        {
            _thread = new Thread(WorkerThreadFunc);
            _thread.Name = "My Worker Thread";
            _thread.IsBackground = true;
            _thread.Start();
            log.Info("Service started");
        }

        protected override void OnStop()
        {
            _shutdownEvent.Set();
            if (!_thread.Join(3000))
            { // give the thread 3 seconds to stop
                _thread.Abort();
                log.Info("Service stopped");
            }
        }

        private void WorkerThreadFunc()
        {
            bool started = false;
            while (!_shutdownEvent.WaitOne(5000))
            {
                if (!started)
                {
                    try
                    {
                        var bbucketConfiguration = JsonConvert.DeserializeObject<BitbucketConfiguration>(File.ReadAllText("bbucketConfig.json"));
                        var botToken = JsonConvert.DeserializeObject<string>(File.ReadAllText("botToken.json"));
                        var tcConfiguration = JsonConvert.DeserializeObject<TeamCityConfiguration>(File.ReadAllText("tcConfig.json"));
                        var bitBucketRepository = new BitBucketRepository(bbucketConfiguration.BbucketKey,
                            bbucketConfiguration.BbucketSecret, bbucketConfiguration.AccountName, bbucketConfiguration.RepositoryName);
                        XmlConfigurator.Configure(new FileInfo("log4net.config"));

                        JiraConfiguration jiraConfiguration = null;
                        if (File.Exists("jiraConfig.json"))
                        {
                            jiraConfiguration = JsonConvert.DeserializeObject<JiraConfiguration>(File.ReadAllText("jiraConfig.json"));
                        }
                        var bot = new MargieBotImplementation(botToken, bitBucketRepository, tcConfiguration, jiraConfiguration);
                        var margieBot = new Bot();
                        var botTask = bot.Run(margieBot);

                        string usersToSlackMapping = File.ReadAllText("usersToSlackMapping.json");
                        //var task = bot.Run();
                        var q = QuartzScheduler.Instance;
                        q.Init(jiraConfiguration, "Medewerkers", usersToSlackMapping, margieBot, hourInterval: 24, allowedDays: 4);
                        q.StartFillWorklogReminder();
                        botTask.Wait();
                        Console.ReadLine();
                        started = true;
                    }
                    catch (Exception ex)
                    {
                        log.Error("Error running service", ex);
                        throw;
                    }
                }
            }
        }
    }
}
